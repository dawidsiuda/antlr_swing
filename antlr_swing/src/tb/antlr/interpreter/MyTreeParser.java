package tb.antlr.interpreter;


import java.util.HashMap;
import java.util.Map;
import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

public class MyTreeParser extends TreeParser {

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected HashMap<String, Integer> symbolMap = new HashMap<String, Integer>();
	
	protected Integer setVariable(String i1, Integer e2) {
		if(symbolMap.containsKey(i1)) {
			symbolMap.put(i1, e2);
			return e2;
		}
		throw new RuntimeException("Variable \"" + i1 + "\" was not initialized");
	}
	
	protected Integer initialize(String i1, Integer e2) {
		if(symbolMap.containsKey(i1)) {
			throw new RuntimeException("Variable \"" + i1 + "\" was already initialized");
		}
		symbolMap.put(i1, e2);
		return e2;
	}
	
	protected Integer initialize(String i1) {
		return this.initialize(i1, 0);
	}
}
